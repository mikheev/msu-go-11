package main

import (
	"./structs"
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	_ "strings"
)

const logsPath = "./data/"

func main() {
	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {

		if req.URL.String() == "/favicon.ico" {
			return
		}

		Search(w)
	}))
	err := http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func Search(w http.ResponseWriter) {
	seenBrowsers := []string{}
	uniqueBrowsers := 0

	r := regexp.MustCompile("@")

	files, _ := ioutil.ReadDir(logsPath)

	for _, f := range files {
		foundUsers := ""

		fmt.Fprintln(w, f.Name())

		filePath := logsPath + f.Name()

		file, err := os.Open(filePath)
		if err != nil {
			panic(err)
		}

		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)

		i := 0
		for scanner.Scan() {
			line := scanner.Text()
			var user structs.UserType
			err := user.UnmarshalJSON([]byte(line))

			if err != nil {
				continue
			}
			fmt.Println(user)

			isAndroid := false
			isMSIE := false

			browsers := user.Browsers

			for _, browser := range browsers {
				if ok, err := regexp.MatchString("Android", browser); ok && err == nil {
					isAndroid = true
					notSeenBefore := true
					for _, item := range seenBrowsers {
						if item == browser {
							notSeenBefore = false
						}
					}
					if notSeenBefore {
						log.Printf("New browser: %s, first seen: %s", browser, user.Name)
						seenBrowsers = append(seenBrowsers, browser)
						uniqueBrowsers++
					}
				}
			}

			for _, browser := range browsers {
				if ok, err := regexp.MatchString("MSIE", browser); ok && err == nil {
					isMSIE = true
					notSeenBefore := true
					for _, item := range seenBrowsers {
						if item == browser {
							notSeenBefore = false
						}
					}
					if notSeenBefore {
						log.Printf("New browser: %s, first seen: %s", browser, user.Name)
						seenBrowsers = append(seenBrowsers, browser)
						uniqueBrowsers++
					}
				}
			}

			if !(isAndroid && isMSIE) {
				continue
			}

			log.Println("Android and MSIE user:", user.Name, user.Email)
			email := r.ReplaceAllString(user.Email, " [at] ")
			foundUsers += fmt.Sprintf("[%d] %s <%s>\n", i, user.Name, email)
			i++
		}

		fmt.Fprintln(w, "found users:\n"+foundUsers)
		fmt.Fprintln(w, "Total unique browsers", uniqueBrowsers, "\n\n")
	}
}
