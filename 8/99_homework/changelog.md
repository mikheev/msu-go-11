1. JSON from easyjson
1a. Removed interfaces
2. Slice of users removed. They were stored in RAM and were not used.
3. Read file line by line, and not save in RAM all file content.
4. SeenBrowsers changed from slice to map for faster search. 
5. Cycles for browser check combined.
6. fmt removed
7. Regexp removed
-----------------
8. String for found user accumulation changed to direct file writing
9. Conversion from byte to string and back to byte before JSON unmarshalling removed