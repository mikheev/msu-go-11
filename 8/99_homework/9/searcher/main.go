package main

import (
	"./structs"
	"bufio"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const logsPath = "./data/"

func main() {
	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {

		if req.URL.String() == "/favicon.ico" {
			return
		}

		Search(w)
	}))
	err := http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func Search(w http.ResponseWriter) {
	seenBrowsers := make(map[string]bool)
	uniqueBrowsers := 0

	files, _ := ioutil.ReadDir(logsPath)

	for _, f := range files {
		w.Write([]byte(f.Name() + "\n"))

		filePath := logsPath + f.Name()

		file, err := os.Open(filePath)
		if err != nil {
			panic(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)

		
		w.Write([]byte("found users:\n"))
		i := 0

		
		var user structs.UserType 
		for scanner.Scan() {			
			err = user.UnmarshalJSON(scanner.Bytes())

			if err != nil {
				continue
			}

			isAndroid := false
			isMSIE := false

			browsers := user.Browsers

			for _, browser := range browsers {
				if strings.Contains(browser, "Android") {
					isAndroid = true
				}

				if strings.Contains(browser, "MSIE") {
					isMSIE = true
				}

				if isAndroid || isMSIE {
					_, seenBefore := seenBrowsers[browser]
					if !seenBefore {
						log.Printf("New browser: %s, first seen: %s", browser, user.Name)
						seenBrowsers[browser] = true
						uniqueBrowsers++
					}
				}
			}

			if !(isAndroid && isMSIE) {
				continue
			}

			log.Println("Android and MSIE user:", user.Name, user.Email)
			email := strings.Replace(user.Email, "@", " [at] ", 1)
			w.Write([]byte("[" + strconv.Itoa(i) + "] " + user.Name + " <" + email + ">\n"))
			i++
		}

		w.Write([]byte("\n"))
		w.Write([]byte("Total unique browsers" + strconv.Itoa(uniqueBrowsers) + "\n\n\n"))
	}
}
