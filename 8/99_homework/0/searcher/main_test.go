package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

// go test -bench=. -cpuprofile cpu.out
// go tool pprof <бинарник(02_pprof.test)> cpu.out

func BenchmarkSubstring(b *testing.B) {
	w := httptest.NewRecorder()
	Search(w)
	_, err := http.Get("http://localhost:8081/")
	if err != nil {
		println(err.Error())
	}
}
