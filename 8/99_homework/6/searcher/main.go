package main

import (
	"./structs"
	"bufio"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
)

const logsPath = "./data/"

func main() {
	http.Handle("/", http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {

		if req.URL.String() == "/favicon.ico" {
			return
		}

		Search(w)
	}))
	err := http.ListenAndServe(":8081", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func Search(w http.ResponseWriter) {
	seenBrowsers := make(map[string]bool)
	uniqueBrowsers := 0

	r := regexp.MustCompile("@")

	files, _ := ioutil.ReadDir(logsPath)

	for _, f := range files {
		foundUsers := ""

		w.Write([]byte(f.Name() + "\n"))

		filePath := logsPath + f.Name()

		file, err := os.Open(filePath)
		if err != nil {
			panic(err)
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)

		i := 0
		for scanner.Scan() {
			line := scanner.Text()
			var user structs.UserType
			err := user.UnmarshalJSON([]byte(line))

			if err != nil {
				continue
			}

			for _, b := range user.Browsers {
				print(b, " ")
			}
			println(user.Company, user.Country, user.Email, user.Job, user.Name, user.Phone)

			isAndroid := false
			isMSIE := false

			browsers := user.Browsers

			for _, browser := range browsers {
				if ok, err := regexp.MatchString("Android", browser); ok && err == nil {
					isAndroid = true
				}

				if ok, err := regexp.MatchString("MSIE", browser); ok && err == nil {
					isMSIE = true
				}

				if isAndroid || isMSIE {
					_, seenBefore := seenBrowsers[browser]
					if !seenBefore {
						log.Printf("New browser: %s, first seen: %s", browser, user.Name)
						seenBrowsers[browser] = true
						uniqueBrowsers++
					}
				}
			}

			if !(isAndroid && isMSIE) {
				continue
			}

			log.Println("Android and MSIE user:", user.Name, user.Email)
			email := r.ReplaceAllString(user.Email, " [at] ")
			foundUsers = foundUsers + "[" + strconv.Itoa(i) + "] " + user.Name + " <" + email + ">\n"
			i++
		}

		w.Write([]byte("found users:\n" + foundUsers + "\n"))
		w.Write([]byte("Total unique browsers" + strconv.Itoa(uniqueBrowsers) + "\n\n\n"))
	}
}
