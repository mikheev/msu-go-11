package main

import (
	"sort"
	"strconv"
)

func main() {
}

func ReturnInt() int {
	m := 1
	return m
}

func ReturnFloat() float32 {
	var k float32 = 1.1
	return k
}

func ReturnIntArray() [3]int {
	arr := [...]int{1, 3, 4}
	return arr
}

func ReturnIntSlice() []int {
	sl := []int{1, 2, 3}
	return sl
}

func IntSliceToString(sl []int) string {
	str := ""
	for _, num := range sl {
		str += strconv.Itoa(num)
	}
	return str
}

func MergeSlices(sl1 []float32, sl2 []int32) []int {
	var sl3 []int
	sl3 = make([]int, len(sl1)+len(sl2), len(sl1)+len(sl2))

	// Slice type casting required
	for idx := range sl1 {
		sl3[idx] = int(sl1[idx])
	}

	for idx := range sl2 {
		sl3[idx+len(sl1)] = int(sl2[idx])
	}

	return sl3
}

func GetMapValuesSortedByKey(dict map[int]string) []string {
	keys := make([]int, len(dict), len(dict))
	strings := make([]string, len(dict), len(dict))
	idx := 0

	// Create slice of keys and sort it
	for key := range dict {
		keys[idx] = key
		idx++
	}
	sort.Ints(keys)

	// Create slice of strings using sorted keys
	for idx, key := range keys {
		strings[idx] = dict[key]
	}

	return strings
}