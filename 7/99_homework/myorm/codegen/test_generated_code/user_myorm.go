package user

import (
	"database/sql"
)

var myormDB *sql.DB

func setNullStr(s string) sql.NullString {
	var ns sql.NullString
	if s != "" {
		ns.String = s
		ns.Valid = true
	} else {
		ns.Valid = true
	}
	return ns
}

func getNullStr(ns sql.NullString) string {
	var s string
	if ns.Valid {
		s = ns.String
	} else {
		s = ""
	}
	return s
}

// SetDB - set database.
// This function should be called
// before other functions from this package
func SetDB(db *sql.DB) {
	myormDB = db
}

// FindByPK - finds row in database by primary key
func (u *User) FindByPK(key interface{}) error {
	var infoNullStr sql.NullString

	row := myormDB.QueryRow("SELECT id, username, info, balance, status FROM users WHERE id = ?", key)
	err := row.Scan(&u.ID, &u.Login, &infoNullStr, &u.Balance, &u.Status)

	u.Info = getNullStr(infoNullStr)

	return err
}

// Create  - creates new row in database
// using data stored in the structure.
// Primary key will be updated according inserted value
func (u *User) Create() error {
	infoNullStr := setNullStr(u.Info)

	result, err := myormDB.Exec(
		"INSERT INTO users (`id`, `username`, `info`, `balance`, `status`) VALUES (?, ?, ?, ?, ?)",
		u.ID,
		u.Login,
		infoNullStr,
		u.Balance,
		u.Status,
	)

	if err != nil {
		return err
	}
	
	lastID, err := result.LastInsertId()
	u.ID = uint(lastID)
	return err
}

// Update  - update data in database
// using data stored in the structure
func (u *User) Update() error {
	infoNullStr := setNullStr(u.Info)

	_, err := myormDB.Exec(
		"UPDATE users SET username = ?, info = ?, balance = ?, status = ? WHERE id = ?",
		u.Login,
		infoNullStr,
		u.Balance,
		u.Status,
		u.ID,
	)
	return err
}
