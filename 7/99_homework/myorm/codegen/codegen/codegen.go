package codegen

import (
	"errors"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

type myormTableType struct {
	structName    string
	tableName     string
	packageName   string
	structType    string
	pkStructField string
	pkDbField     string
	dbFields      []string
	nullDbFields  []string
	structFields  []string
}

// Generate - generate database access for structs stored in file
func Generate(structFilePath string, structFileName string) error {
	var myTable myormTableType
	var tables []myormTableType // Здесь будут храниться все найденные в файле структуры и, соответственно, таблицы

	// Для работы ast нам нужно инициализровать набор файлов
	fset := token.NewFileSet()
	// Парс файла
	//f, err := parser.ParseFile(fset, `..\test\tablesTest2.go`, nil, parser.ParseComments)
	f, err := parser.ParseFile(fset, structFilePath+structFileName, nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	// Decls содержит в себе список всех объявленных переменных и функций
	for _, decl := range f.Decls {
		// В файле может быть много всего. Если что-то не плоучилось - это не ошибка.
		// Мы пытаемся делать то, что нам делать не нужно
		// Поэтому просто идём дальше
		typeDecl, ok := decl.(*ast.GenDecl)
		if !ok {
			//fmt.Println("Not our decl!")
			continue
		}

		structDecl, ok := typeDecl.Specs[0].(*ast.TypeSpec).Type.(*ast.StructType)
		if !ok {
			//fmt.Println(typeDecl.Specs[0].(*ast.TypeSpec).Name, "not our decl!!!")
			continue
		}

		// Достаём комментарий к структуре
		structComment := typeDecl.Doc.Text()
		if !strings.Contains(structComment, "myorm") {
			//fmt.Println("Not our struct")
			continue
		}

		// Достаём имя таблицы
		structCommentParts := strings.Split(structComment, ":")
		if len(structCommentParts) != 2 {
			errStr := fmt.Sprint("Error! Bad syntax:", structCommentParts)
			return errors.New(errStr)
		}

		myTable.tableName = structCommentParts[1][:len(structCommentParts[1])-1]
		myTable.structName = typeDecl.Specs[0].(*ast.TypeSpec).Name.Name
		//fmt.Println("This struct will be stored in table", myTable.tableName)

		///// Собираем данные для генерации
		myTable.structType = typeDecl.Specs[0].(*ast.TypeSpec).Name.Name
		myTable.packageName = f.Name.Name
		fields := structDecl.Fields.List

		// Счётчик ключей (который должен быть всего один)
		pkcnt := 0
		for _, field := range fields {
			//fmt.Println(field.Type, field.Names[0])

			structFieldName := field.Names[0].Name
			dbFieldName := strings.ToLower(field.Names[0].Name)
			nullDbFieldName := ""

			if field.Tag != nil {
				//fmt.Println(field.Tag.Value)
				addField := true

				tagsRawStr := field.Tag.Value
				if !strings.Contains(tagsRawStr, "myorm") {
					continue
				}

				tagsRawStr = tagsRawStr[len("`myorm:\"") : len(tagsRawStr)-2]
				tags := strings.Split(tagsRawStr, ";")

				for _, tag := range tags {
					tagFields := strings.Split(tag, ":")
					//fmt.Println(tagFields)

					switch tagFields[0] {
					case "-":
						addField = false
					case "primary_key":
						pkcnt++
						myTable.pkStructField = field.Names[0].Name
						myTable.pkDbField = strings.ToLower(field.Names[0].Name)
					case "null":
						nullDbFieldName = strings.ToLower(field.Names[0].Name) + "NullStr"
					case "column":
						dbFieldName = tagFields[1]
					}
				}

				if !addField {
					continue
				}
			}

			myTable.structFields = append(myTable.structFields, structFieldName)
			myTable.dbFields = append(myTable.dbFields, dbFieldName)
			myTable.nullDbFields = append(myTable.nullDbFields, nullDbFieldName)
		}

		// Проверяем, что у нас ровно один ключ
		if pkcnt == 0 {
			return errors.New("In struct " + myTable.structName + " primary key specified.")
		} else if pkcnt > 1 {
			return errors.New("In struct " + myTable.structName + " more than one primary key")
		}

		tables = append(tables, myTable)
	}

	if len(tables) < 1 {
		return errors.New("No structs to generate code")
	}

	///// Генерация кода
	// Шапка. Одна на всех
	srcGenerated := "package " + f.Name.Name + "\n"
	srcGenerated += `
import (
	"database/sql"
)

var myormDB *sql.DB

func setNullStr(s string) sql.NullString {
	var ns sql.NullString
	if s != "" {
		ns.String = s
		ns.Valid = true
	} else {
		ns.Valid = true
	}
	return ns
}

func getNullStr(ns sql.NullString) string {
	var s string
	if ns.Valid {
		s = ns.String
	} else {
		s = ""
	}
	return s
}

// SetDB - set database.
// This function should be called
// before other functions from this package
func SetDB(db *sql.DB) {
	myormDB = db
}
`

	// Генерируем функции
	for _, table := range tables {
		srcGenerated += generateFindByPKFunc(&table)
		srcGenerated += generateCreateFunc(&table)
		srcGenerated += generateUpdateFunc(&table)
	}

	// Сохраняем рузультаты в файл
	outFileName := f.Name.Name + "_myorm.go"
	err = ioutil.WriteFile(structFilePath+outFileName, []byte(srcGenerated), os.ModeAppend)
	PanicOnErr(err)

	return nil
}

/////////////   Функции для генерации функций

func generateFindByPKFunc(myTable *myormTableType) string {
	src := "\n// FindByPK - finds row in database by primary key\n"
	src += "func (u *" + myTable.structType + ") FindByPK(key interface{}) error {\n"

	for _, nullStrName := range myTable.nullDbFields {
		if nullStrName != "" {
			src += "\tvar " + nullStrName + " sql.NullString\n"
		}
	}
	src += "\n"

	src += "\trow := myormDB.QueryRow(\"SELECT "
	for idx, field := range myTable.dbFields {
		src += field
		if idx < len(myTable.dbFields)-1 {
			src += ", "
		}
	}

	src += " FROM " + myTable.tableName + " WHERE " + myTable.pkDbField + " = ?\", key)\n"

	src += "\terr := row.Scan("
	for idx, field := range myTable.structFields {
		if myTable.nullDbFields[idx] == "" {
			src += "&u." + field
		} else {
			src += "&" + myTable.nullDbFields[idx]
		}

		if idx < len(myTable.structFields)-1 {
			src += ", "
		} else {
			src += ")\n"
		}

	}

	src += "\n"
	for idx, nullStrName := range myTable.nullDbFields {
		if nullStrName != "" {
			src += "\tu." + myTable.structFields[idx] + " = getNullStr(" + nullStrName + ")"
		}
	}
	src += "\n"
	src += "\n"

	src += "	return err\n"
	src += "}\n"

	return src
}

func generateCreateFunc(myTable *myormTableType) string {
	src := "\n// Create  - creates new row in database\n"
	src += "// using data stored in the structure.\n"
	src += "// Primary key will be updated according inserted value\n"
	src += "func (u *" + myTable.structType + ") Create() error {\n"

	for idx, nullStrName := range myTable.nullDbFields {
		if nullStrName != "" {
			src += "\t" + nullStrName + " := setNullStr(u." + myTable.structFields[idx] + ")\n"
		}
	}
	src += "\n"

	src += "\tresult, err := myormDB.Exec(\n"
	src += "\t\t\"INSERT INTO " + myTable.tableName + " ("
	for idx, field := range myTable.dbFields {
		src += "`" + field + "`"
		if idx < len(myTable.dbFields)-1 {
			src += ", "
		} else {
			src += ") VALUES ("
			for idx2 := range myTable.dbFields {
				if idx2 < len(myTable.dbFields)-1 {
					src += "?, "
				} else {
					src += "?)\",\n"
				}
			}
		}
	}

	for idx, field := range myTable.structFields {
		if myTable.nullDbFields[idx] == "" {
			src += "\t\tu." + field + ",\n"
		} else {
			src += "\t\t" + myTable.nullDbFields[idx] + ",\n"
		}

	}

	src += `	)

	if err != nil {
		return err
	}
	
	lastID, err := result.LastInsertId()
`
	src += "\tu." + myTable.pkStructField + " = uint(lastID)\n"
	src += "\treturn err\n"
	src += "}\n"

	return src
}

func generateUpdateFunc(myTable *myormTableType) string {
	src := "\n// Update  - update data in database\n"
	src += "// using data stored in the structure\n"
	src += "func (u *" + myTable.structType + ") Update() error {\n"

	for idx, nullStrName := range myTable.nullDbFields {
		if nullStrName != "" {
			src += "\t" + nullStrName + " := setNullStr(u." + myTable.structFields[idx] + ")\n"
		}
	}
	src += "\n"

	src += "\t_, err := myormDB.Exec(\n"
	src += "\t\t\"UPDATE " + myTable.tableName + " SET "
	for idx, field := range myTable.dbFields {
		if field != myTable.pkDbField {
			src += field + " = ?"
			if idx < len(myTable.dbFields)-1 {
				src += ", "
			} else {
				src += " WHERE " + myTable.pkDbField + " = ?\",\n"
			}
		}
	}

	for idx, field := range myTable.structFields {
		if field != myTable.pkStructField {
			if myTable.nullDbFields[idx] == "" {
				src += "\t\tu." + field + ",\n"
			} else {
				src += "\t\t" + myTable.nullDbFields[idx] + ",\n"
			}
		}
	}
	src += "\t\tu." + myTable.pkStructField + ",\n"

	src += `	)
	return err
}
`
	return src
}

//PanicOnErr panics on error
func PanicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}
