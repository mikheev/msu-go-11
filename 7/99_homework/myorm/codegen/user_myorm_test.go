package main

import (
	"./test_generated_code"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"testing"
)

func TestFindByPk(t *testing.T) {
	db := connectToTestDB()
	user.SetDB(db)

	var u user.User
	u.SomeInnerFlag = true

	// Test valid ID
	err := u.FindByPK(1)
	if err != nil {
		t.Errorf("FindByPK returned error: "+err.Error(), ". Expected nil")
	}

	var refUser user.User
	refUser.ID = 1
	refUser.Login = "Vasily Romanov"
	refUser.Info = "company: Mail.ru Group"
	refUser.Balance = 100500
	refUser.Status = 1

	if u.ID != refUser.ID || u.Login != refUser.Login || u.Info != refUser.Info || u.Status != refUser.Status {
		responseStr := fmt.Sprint("Content error: Expected ", refUser, " got ", u)
		t.Errorf(responseStr)
	}

	// Test invalid ID
	err = u.FindByPK(100500)
	expErrorStr := "sql: no rows in result set"
	if err.Error() != expErrorStr {
		responseStr := fmt.Sprint("Invalid ID test. Expected error ", expErrorStr, " got ", err.Error())
		t.Errorf(responseStr)
	}

	if u.SomeInnerFlag != true {
		t.Errorf("Fields, not used in DB corrupted")
	}
}

func TestCreate(t *testing.T) {
	db := connectToTestDB()
	user.SetDB(db)

	var refUser user.User
	refUser.ID = 0
	refUser.Login = "Ivan Petrov"
	refUser.Info = "company: Roga&Copita"
	refUser.Balance = 100500
	refUser.Status = 100

	err := refUser.Create()

	if err != nil {
		t.Errorf("Got error: " + err.Error())
	}

	if refUser.ID == 0 {
		t.Errorf("Primary key wasn't updated")
	}

	var u user.User

	err = u.FindByPK(refUser.ID)
	if err != nil {
		t.Errorf("Last insert ID invalid")
	}

	if u.ID != refUser.ID || u.Login != refUser.Login || u.Info != refUser.Info || u.Status != refUser.Status {
		responseStr := fmt.Sprint("Content error: Inserted content ", refUser, " got content from DB", u)
		t.Errorf(responseStr)
	}

	refUser.Login = "\""
	err = refUser.Create()
	expErrorStr := fmt.Sprint("Error 1062: Duplicate entry '", u.ID, "' for key 'PRIMARY'")
	if err.Error() != expErrorStr {
		responseStr := fmt.Sprint("Invalid parameter test. Expected error ", expErrorStr, " got ", err.Error())
		t.Errorf(responseStr)
	}
}

func TestUpdate(t *testing.T) {
	db := connectToTestDB()
	user.SetDB(db)

	var refUser user.User
	err := refUser.FindByPK(2)

	refUser.Login = refUser.Login + "a"
	refUser.Info = refUser.Info + "a"
	refUser.Balance++
	refUser.Status++

	err = refUser.Update()
	if err != nil {
		t.Errorf("Got error after updated: " + err.Error())
	}

	var u user.User
	err = u.FindByPK(refUser.ID)
	if err != nil {
		t.Errorf("Got error on getting updated entry: " + err.Error())
	}

	if u.ID != refUser.ID || u.Login != refUser.Login || u.Info != refUser.Info || u.Status != refUser.Status {
		responseStr := fmt.Sprint("Content error: Updayed content: ", refUser, " got content from DB", u)
		t.Errorf(responseStr)
	}
}

func connectToTestDB() *sql.DB {
	db, _ := sql.Open("mysql", "root@tcp(localhost:3306)/myorm_test_db?charset=utf8&interpolateParams=true")
	db.SetMaxOpenConns(10)
	_ = db.Ping()
	return db
}

func TestNullStr(t *testing.T) {
	db := connectToTestDB()
	user.SetDB(db)

	var refUser user.User
	refUser.ID = 0
	refUser.Login = "Ivan Petrov"
	refUser.Info = ""
	refUser.Balance = 100500
	refUser.Status = 100

	err := refUser.Create()

	if err != nil {
		t.Errorf("Create got error: " + err.Error())
	}

	err = refUser.Update()
	if err != nil {
		t.Errorf("Update got error: " + err.Error())
	}

	var u user.User
	err = u.FindByPK(refUser.ID)
	if err != nil {
		t.Errorf("FindByPK got error: " + err.Error())
	}

}
