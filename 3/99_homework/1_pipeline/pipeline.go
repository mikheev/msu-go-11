package pipeline

import "sync"

type job func(in, out chan interface{})

func doJob(in, out chan interface{}, funcToDo job, ww *sync.WaitGroup) {
	funcToDo(in, out)
	close(out)
	ww.Done()
}

func Pipe(funcs ...job) {
	var chans []chan interface{}
	c := make(chan interface{})
	chans = append(chans, c)
	close(chans[0])

	var wg sync.WaitGroup

	for idx, funcToDo := range funcs {
		c := make(chan interface{})
		chans = append(chans, c)

		wg.Add(1)
		go doJob(chans[idx], chans[idx+1], funcToDo, &wg)
	}
	wg.Wait()
}
