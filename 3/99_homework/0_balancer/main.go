package main

import "sync/atomic"

type RoundRobinBalancer struct {
	numNodes uint64
	nextNode uint64
	numTasks []uint64
}

func (rbb *RoundRobinBalancer) Init(numNodes int) {
	rbb.numNodes = uint64(numNodes)
	rbb.numTasks = make([]uint64, numNodes, numNodes)
	for i := range rbb.numTasks {
		rbb.numTasks[i] = 0
	}
	rbb.nextNode = 0
}

func (rbb *RoundRobinBalancer) GiveNode() uint64 {
	node := (atomic.AddUint64(&rbb.nextNode, 1) - 1) % rbb.numNodes
	atomic.AddUint64(&rbb.numTasks[node], 1)
	return node
}

func (rbb RoundRobinBalancer) GiveStat() []int {
	stat := make([]int, rbb.numNodes, rbb.numNodes)
	for i, num := range rbb.numTasks {
		stat[i] = int(num)
	}
	return stat
}

func main() {
}
