package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	_ "github.com/lib/pq"
	"reflect"
	"time"
)

// --------- Data types
type cacheItem struct {
	Data interface{} // можем класть любые данные
	Tags map[string]int
}

type article struct {
	Name string
	From string
}

type articles []article

type buildCacheFuncType func() (interface{}, []string)

// --------- Global variables
var c redis.Conn
var db *sql.DB

// --------- Postgres
func createTableForData(db *sql.DB) {
	//_, err := db.Query("CREATE TABLE storage (id serial NOT NULL, data text NOT NULL);")
	_, err := db.Query("CREATE TABLE cache (id serial NOT NULL, data text NOT NULL);")
	PanicOnErr(err)
}

func insertDataIntoDB(data string) {
	stmt, err := db.Prepare("INSERT INTO storage (data) VALUES ($1) RETURNING id")
	PanicOnErr(err)
	_, err = stmt.Exec(data)
	PanicOnErr(err)
}

func getDataFromDB(id int64) (string, error) {
	println("call SQL")
	var data string
	row := db.QueryRow("SELECT data FROM storage WHERE id = $1", id)
	err := row.Scan(&data)
	return data, err
}

// --------- Redis
func getCachedData(mkey string) (string, error) {
	println("redis get", mkey)
	// получает запись, https://redis.io/commands/get
	data, err := c.Do("GET", mkey)
	item, err := redis.String(data, err)
	// если записи нету, то для этого есть специальная ошибка, её надо обрабатывать отдеьно, это почти штатная ситуация, а не что-то страшное
	if err == redis.ErrNil {
		fmt.Println("Record not found in redis (return value is nil)")
		return "", redis.ErrNil
	} else if err != nil {
		return "", err
	}
	return item, nil
}

// --------- Cache logic
func buildCache() (interface{}, []string) {
	rawData, _ := getDataFromDB(1)

	var data articles
	_ = json.Unmarshal([]byte(rawData), &data)
	var tagNames []string
	for _, article := range data {
		tagNames = append(tagNames, article.From)
	}
	return rawData, tagNames
}

func TcacheGet(mkey string, buildCache buildCacheFuncType) cacheItem {

	var dataFromCache string
	var err error

	// если кеш есть, то мы выходим сразу, если нет - у нас 4 попытки получить значение
	for i := 0; i < 4; i++ {
		dataFromCache, err = getCachedData(mkey)

		if err != nil {
			if err != redis.ErrNil {
				PanicOnErr(err)
			}
			// Если err = redis.ErrNil - записи нет
			// просто будем перестраивать кег

		} else {
			// запись нашлась
			// Нужно проверять актуальность

			// Достаём данные и исходные значения тегов из кеша
			cItems := cacheItem{}
			fmt.Println("top Cache", dataFromCache)
			_ = json.Unmarshal([]byte(dataFromCache), &cItems)
			toCompare := make([]int, 0)
			keys := make([]interface{}, 0)
			fmt.Println(cItems.Tags)
			for key, val := range cItems.Tags {
				keys = append(keys, key)
				toCompare = append(toCompare, val)
			}

			// Достаём значения тегов, которые могли быть изменены
			reply, err := redis.Ints(c.Do("MGET", keys...))
			PanicOnErr(err)

			fmt.Println("compare cached tag values", toCompare, "with current tag values", reply)

			if reflect.DeepEqual(toCompare, reply) {
				println("Cache is valid. No rebuild needed")
				return cItems
			}
		}

		// Записи по кешу не нашлось или нашлось, но значения тегов изменились
		// Будем перестраивать кеш
		// пытаемся сказать "я строю этот кеш, другие - ждите"
		lockStatus, _ := redis.String(c.Do("SET", mkey+"_lock", dataFromCache, "EX", 3, "NX"))
		if lockStatus != "OK" {
			// кто-то другой держит лок, подождём и попробуем получить запись еще раз
			println("sleep", i)
			time.Sleep(time.Millisecond * 10)
		} else {
			// успешло залочились, можем строить кеш
			break
		}
	}

	// Перестраиваем кеш
	println("Rebuild cache")

	// Достаём актуальные данные из хранилища
	dataFromDB, tagNames := buildCache()

	var dataToCache cacheItem
	dataToCache.Data = dataFromDB
	dataToCache.Tags = make(map[string]int)
	for _, tagName := range tagNames {
		// Доста актуальные значения тегов
		tagVal, err := redis.Int(c.Do("GET", tagName))
		// если записи нету, то для этого есть специальная ошибка, её надо обрабатывать отдеьно, это почти штатная ситуация, а не что-то страшное
		if err == redis.ErrNil {
			// Такого тега ещё нет
			dataToCache.Tags[tagName] = 1
			c.Do("SET", tagName, 1)
		} else if err != nil {
			PanicOnErr(err)
		} else {
			dataToCache.Tags[tagName] = tagVal
		}
	}
	dataToCacheRaw, _ := json.Marshal(dataToCache)
	ttl := 50
	result, err := redis.String(c.Do("SET", mkey, dataToCacheRaw, "EX", ttl))
	PanicOnErr(err)
	if result != "OK" {
		panic("result not ok: " + result)
	}

	// удаляем лок на построение
	n, err := redis.Int(c.Do("DEL", mkey+"_lock"))
	PanicOnErr(err)
	println("lock deleted:", n)

	return dataToCache
}

// ---------- Main
func main() {
	// ------------ Postgres DB  -----------------
	var err error
	dbURL := "postgres://ehmkfboexesees:9fea8a796f92091e7a4f94829b1a296669892f7c60054ebf5b3ce9f1215bc0b8@ec2-79-125-118-221.eu-west-1.compute.amazonaws.com:5432/d4edufc4s4mrjp"
	db, err = sql.Open("postgres", dbURL)
	if err != nil {
		println(err)
	}
	err = db.Ping()
	PanicOnErr(err)
	//createTableForData(db)

	/*
		// Add data to db
		top := articles{
			article{"Джава и Докер - это должен знать каждый", "Хабр"},
			article{"Как взрываются базовые станиции", "Гиктаймс"},
		}
		dataToDB, _ := json.Marshal(top)
		insertDataIntoDB(string(dataToDB))
	*/

	/*
		// Show all data from DB
		var id int
		var data string
		rows, err := db.Query("SELECT * FROM storage")
		PanicOnErr(err)
		for rows.Next() {
			err = rows.Scan(&id, &data)
			println("id:", id, "data:", data)
		}
		rows.Close()
	*/

	/*
		// Check getDataFromDB()
		data, _ := getDataFromDB(1)
		println(data)
	*/

	// ----------- Redis DB ----------------------
	// соединение
	c, err = redis.DialURL("redis://redis-10242.c9.us-east-1-2.ec2.cloud.redislabs.com:10242")
	PanicOnErr(err)
	// Потом  нужно будет закрыть
	defer c.Close()

	//data, tags := buildCache()
	//fmt.Println(data, tags)

	//c.Do("INCR", "Хабр")
	_ = TcacheGet("top_news_mobile", buildCache)
}

// PanicOnErr panics on error
func PanicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}
