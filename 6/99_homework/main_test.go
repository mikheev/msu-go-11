package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"
)

// Test control variables
type testParamsType struct {
	verySlowResponse     bool // Default - false
	httpRedirectResponse bool //  Default - false
	invalidJSON          bool //  Default - false
}

var testParams testParamsType

// Structures for unmarshalling dataset
type idType struct {
	XMLName xml.Name `xml:"id"`
	Id      int      `xml:",chardata"`
}

type ageType struct {
	XMLName xml.Name `xml:"age"`
	Age     int      `xml:",chardata"`
}

type nameType struct {
	XMLName xml.Name `xml:"first_name"`
	Name    string   `xml:",chardata"`
}

type aboutType struct {
	XMLName xml.Name `xml:"about"`
	Text    string   `xml:",chardata"`
}

type genderType struct {
	XMLName xml.Name `xml:"gender"`
	Text    string   `xml:",chardata"`
}

type rowType struct {
	XMLName xml.Name `xml:"row"`
	Id      idType
	Age     ageType
	Name    nameType
	About   aboutType
	Gender  genderType
}

type XMLUserDataType struct {
	XMLName xml.Name  `xml:"root"`
	Rows    []rowType `xml:"row"`
}

// Webserver emulator for tests
func Case(f func(searcherURL string, limit int, offset int, query string, orderField string, orderBy int) (*SearchResponse, error),
	limit int, offset int, query string, orderField string, orderBy int) (*SearchResponse, error) {

	ts := httptest.NewServer(
		http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				//req := r.RequestURI

				// load dataset
				dataSet, err := ioutil.ReadFile("dataset.xml")
				if err != nil {
					fmt.Println("Error opening file:", err)
					return
				}

				//println(string(dataSet))

				XMLUserData := XMLUserDataType{}

				err = xml.Unmarshal(dataSet, &XMLUserData)

				// Параметры поиска
				searchParams := r.URL.Query()
				query := searchParams.Get("query")

				limit, _ := strconv.Atoi(searchParams.Get("limit"))
				offset, _ := strconv.Atoi(searchParams.Get("offset"))
				orderField := searchParams.Get("order_field")
				orderBy, _ := strconv.Atoi(searchParams.Get("order_by"))

				var users []User
				var tempUser User
				for _, row := range XMLUserData.Rows {
					tempUser.About = row.About.Text
					tempUser.Age = row.Age.Age
					tempUser.Gender = row.Gender.Text
					tempUser.Id = row.Id.Id
					tempUser.Name = row.Name.Name

					if (query == "") || strings.Contains(tempUser.Name, query) || strings.Contains(tempUser.About, query) {
						users = append(users, tempUser)
					}

				}

				if orderField == "id" {
					sort.Slice(users, func(i, j int) bool {
						if orderBy == orderAsc {
							return users[i].Id < users[j].Id
						}
						return users[i].Id > users[j].Id
					})
				} else if orderField == "age" {
					sort.Slice(users, func(i, j int) bool {
						if orderBy == orderAsc {
							return users[i].Age < users[j].Age
						}
						return users[i].Age > users[j].Age
					})
				} else { //orderField == "name" or orderField == ""
					sort.Slice(users, func(i, j int) bool {
						if orderBy == orderAsc {
							return users[i].Name < users[j].Name
						}
						return users[i].Name > users[j].Name
					})
				}

				var usersOut []User
				var maxIdx int
				if len(users) > offset+limit {
					maxIdx = offset + limit
				} else {
					maxIdx = len(users)
				}

				for idx := offset; idx < maxIdx; idx++ {
					usersOut = append(usersOut, users[idx])
				}

				responseData, _ := json.Marshal(usersOut)

				///////  Test features

				if testParams.verySlowResponse {
					time.Sleep(1100 * time.Millisecond)
				}

				if testParams.httpRedirectResponse {
					http.Redirect(w, r, "/page1.html", 301)
				}

				if testParams.invalidJSON {
					responseData = []byte(string(responseData) + "{[[}")
				}

				w.Write([]byte(responseData))
				/*
					println(r.RequestURI, r.Host)
					if req == "/" {
						println(r.RequestURI, r.Host)
						http.Redirect(w, r, "/page1.html", 301)
					} else {
						http.Redirect(w, r, "/page1.html", 301)
						http.ServeFile(w, r, "testdata/"+root+"/"+req)
					}
				*/
			}))
	defer ts.Close()
	return f(ts.URL, limit, offset, query, orderField, orderBy)
}

func TestResponseStructure(t *testing.T) {
	limit := 10
	offset := 0
	query := "Gilmore"
	orderField := "id"
	orderBy := orderAsc

	var expUser User
	expUser.About = "Labore consectetur do sit et mollit non incididunt. Amet aute voluptate enim et sit Lorem elit. Fugiat proident ullamco ullamco sint pariatur deserunt eu nulla consectetur culpa eiusmod. Veniam irure et deserunt consectetur incididunt ad ipsum sint. Consectetur voluptate adipisicing aute fugiat aliquip culpa qui nisi ut ex esse ex. Sint et anim aliqua pariatur.\n"
	expUser.Age = 32
	expUser.Gender = "male"
	expUser.Id = 11
	expUser.Name = "Gilmore"

	resp, err := Case(doSearch, limit, offset, query, orderField, orderBy)
	if err != nil {
		responseStr := fmt.Sprint("Searched 'Gilmore'. Expected", expUser, "got error", err)
		t.Errorf(responseStr)
	}

	if len(resp.Users) != 1 {
		responseStr := fmt.Sprint("Searched 'Gilmore'. Expected single User, got len(resp.Users) ", len(resp.Users))
		t.Errorf(responseStr)
	}

	//if !reflect.DeepEqual(expUser, resp.Users[0]) {
	if expUser.About != resp.Users[0].About {
		responseStr := fmt.Sprint("Searched 'Gilmore'. Expected About: ", expUser.About[0:10], " got ", resp.Users[0].About[0:10])

		t.Errorf(responseStr)
	}

	if expUser.Age != resp.Users[0].Age {
		responseStr := fmt.Sprint("Searched 'Gilmore'. Expected Age: ", expUser.Age, " got: ", resp.Users[0].Age)

		t.Errorf(responseStr)
	}

	if expUser.Gender != resp.Users[0].Gender {
		responseStr := fmt.Sprint("Searched 'Gilmore'. Expected Gender: ", expUser.Gender, " got:  ", resp.Users[0].Gender)

		t.Errorf(responseStr)
	}

	if expUser.Id != resp.Users[0].Id {
		responseStr := fmt.Sprint("Searched 'Gilmore'. Expected Id: ", expUser.Id, " got: ", resp.Users[0].Id)

		t.Errorf(responseStr)
	}

	if expUser.Name != resp.Users[0].Name {
		responseStr := fmt.Sprint("Searched 'Gilmore'. Expected Name: ", expUser.Name, " got: ", resp.Users[0].Name)

		t.Errorf(responseStr)
	}

}

func TestSearch(t *testing.T) {
	limit := 24
	offset := 0
	query := "Beulah"
	orderField := "id"
	orderBy := orderAsc

	var recvIds []int

	// Single match
	resp, _ := Case(doSearch, limit, offset, query, orderField, orderBy)
	expIds := []int{5}
	for _, user := range resp.Users {
		recvIds = append(recvIds, user.Id)
	}
	if !reflect.DeepEqual(recvIds, expIds) {
		responseStr := fmt.Sprint("Searched 'Beulah' in names and about fields. Expected", expIds, "got", recvIds)
		t.Errorf(responseStr)
	}

	// Multiple match
	query = "il"
	resp, _ = Case(doSearch, limit, offset, query, orderField, orderBy)
	recvIds = []int{}
	expIds = []int{0, 1, 2, 3, 5, 8, 10, 11, 13, 15, 17, 18, 19, 22, 24, 26, 27, 31, 32, 33, 34}
	for _, user := range resp.Users {
		recvIds = append(recvIds, user.Id)
	}
	if !reflect.DeepEqual(recvIds, expIds) {
		responseStr := fmt.Sprint("Searched 'li' in names and about fields. Expected", expIds, "got", recvIds)
		t.Errorf(responseStr)
	}

	// No match
	query = "wqerwqer"
	resp, _ = Case(doSearch, limit, offset, query, orderField, orderBy)
	recvIds = []int{}
	expIds = []int{}
	for _, user := range resp.Users {
		recvIds = append(recvIds, user.Id)
	}
	if !reflect.DeepEqual(recvIds, expIds) {
		responseStr := fmt.Sprint("Searched 'li' in names and about fields. Expected", expIds, "got", recvIds)
		t.Errorf(responseStr)
	}
}

func TestOrdering(t *testing.T) {
	limit := 10
	offset := 0
	query := "" //Boyd" //"" //"Vasya"

	orderField := "id"
	orderBy := orderAsc
	resp, _ := Case(doSearch, limit, offset, query, orderField, orderBy)
	if resp.Users[0].Id != 0 {
		t.Error("Order by id ASC error: resp.Users[0].Id expected 0, got ", resp.Users[0].Id)
	}

	orderBy = orderDesc
	resp, _ = Case(doSearch, limit, offset, query, orderField, orderBy)
	if resp.Users[0].Id != 34 {
		t.Error("Order by id DESC error: resp.Users[0].Id expected 34, got ", resp.Users[0].Id)
	}

	orderField = "age"
	orderBy = orderAsc
	resp, _ = Case(doSearch, limit, offset, query, orderField, orderBy)
	if resp.Users[0].Age != 21 {
		t.Error("Order by age ASC error: resp.Users[0].Age expected 21, got ", resp.Users[0].Age)
	}

	orderBy = orderDesc
	resp, _ = Case(doSearch, limit, offset, query, orderField, orderBy)
	if resp.Users[0].Age != 40 {
		t.Error("Order by age DESC error: resp.Users[0].Age expected 40, got ", resp.Users[0].Age)
	}

	orderField = "name"
	orderBy = orderAsc
	resp, _ = Case(doSearch, limit, offset, query, orderField, orderBy)
	if resp.Users[0].Name != "Allison" {
		t.Error("Order by name ASC error: resp.Users[0].Name expected Allison, got ", resp.Users[0].Name)
	}

	orderBy = orderDesc
	resp, _ = Case(doSearch, limit, offset, query, orderField, orderBy)
	if resp.Users[0].Name != "Whitley" {
		t.Error("Order by name DESC error: resp.Users[0].Id expected 34, got ", resp.Users[0].Name)
	}
}

func TestNegativeLimitError(t *testing.T) {
	limit := -5
	offset := 0
	query := "Boyd"
	orderField := "id"
	orderBy := 1

	_, err := Case(doSearch, limit, offset, query, orderField, orderBy)
	if err != nil {
		if err.Error() != "limit must be > 0" {
			t.Error("Expected error: 'limit must be > 0', got error: ", err)
		}
	} else {
		t.Error("Expected error: 'limit must be > 0', got error nothing")
	}
}

func TestOverNormalLimit(t *testing.T) {
	limit := 10
	offset := 0
	query := ""
	orderField := "id"
	orderBy := 1

	resp, err := Case(doSearch, limit, offset, query, orderField, orderBy)

	if err != nil {
		if err.Error() != "offset must be > 0" {
			t.Error("Got unexpected error:", err)
		}
	}

	if len(resp.Users) != 10 {
		t.Error("Expected 10 users, got", len(resp.Users))
	}
}

func TestNextPageNotExist(t *testing.T) {
	limit := 10
	offset := 0
	query := "Boyd"
	orderField := "id"
	orderBy := 1

	resp, err := Case(doSearch, limit, offset, query, orderField, orderBy)

	if err != nil {
		if err.Error() != "offset must be > 0" {
			t.Error("Got unexpected error:", err)
		}
	}

	if resp.NextPage {
		t.Error("Expected NextPage = false, got", resp.NextPage)
	}
}

func TestNextPageExist(t *testing.T) {
	limit := 30
	offset := 0
	query := ""
	orderField := "id"
	orderBy := 1

	resp, err := Case(doSearch, limit, offset, query, orderField, orderBy)

	if err != nil {
		if err.Error() != "offset must be > 0" {
			t.Error("Got unexpected error:", err)
		}
	}

	if !resp.NextPage {
		t.Error("Expected NextPage = true, got", resp.NextPage)
	}
}

func TestOver25Limit(t *testing.T) {
	limit := 30
	offset := 0
	query := ""
	orderField := "id"
	orderBy := 1

	resp, err := Case(doSearch, limit, offset, query, orderField, orderBy)

	if err != nil {
		if err.Error() != "offset must be > 0" {
			t.Error("Got unexpected error:", err)
		}
	}

	if len(resp.Users) != 25 {
		t.Error("Expected 25 users, got", len(resp.Users))
	}
}

func TestNormalOffset(t *testing.T) {
	limit := 25
	offset := 2
	query := ""
	orderField := "id"
	orderBy := orderAsc

	var recvIds []int

	resp, _ := Case(doSearch, limit, offset, query, orderField, orderBy)
	expIds := []int{2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26}
	for _, user := range resp.Users {
		recvIds = append(recvIds, user.Id)
	}
	if !reflect.DeepEqual(recvIds, expIds) {
		responseStr := fmt.Sprint("Searched 'li' in names and about fields. Expected", expIds, "got", recvIds)
		t.Errorf(responseStr)
	}
}

func TestNegativeOffsetError(t *testing.T) {
	limit := 10
	offset := -5
	query := "Boyd"
	orderField := "id"
	orderBy := 1

	_, err := Case(doSearch, limit, offset, query, orderField, orderBy)

	if err != nil {
		if err.Error() != "offset must be > 0" {
			t.Error("Expected error: 'offset must be > 0', got error:", err)
		}
	} else {
		t.Error("Expected error: 'limit must be > 0', got nothing")
	}
}

func TestInvalidURL(t *testing.T) {
	limit := 10
	offset := 0
	query := "Boyd"
	orderField := "id"
	orderBy := 1

	_, err := doSearch("https://nosuchserver1234.com", limit, offset, query, orderField, orderBy)

	expectedErrorStr := "Get https://nosuchserver1234.com?limit=11&offset=0&order_by=1&order_field=id&query=Boyd: dial tcp: lookup nosuchserver1234.com: no such host"
	if err != nil {
		if err.Error() != expectedErrorStr {
			t.Error("Expected", expectedErrorStr, "got error:", err)
		}
	} else {
		t.Error("Expected error:", expectedErrorStr, "got nothing")
	}
}

func TestTimeoutError(t *testing.T) {
	limit := 10
	offset := 0
	query := "Boyd"
	orderField := "id"
	orderBy := 1

	testParams.verySlowResponse = true
	_, err := Case(doSearch, limit, offset, query, orderField, orderBy)
	testParams.verySlowResponse = false

	expectedErrorStr := "timeout for limit=11&offset=0&order_by=1&order_field=id&query=Boyd"
	if err != nil {
		if err.Error() != expectedErrorStr {
			t.Error("Expected", expectedErrorStr, "got error:", err)
		}
	} else {
		t.Error("Expected error:", expectedErrorStr, "got nothing")
	}
}

func TestHttpRedirectError(t *testing.T) {
	limit := 10
	offset := 0
	query := "Boyd"
	orderField := "id"
	orderBy := 1

	testParams.httpRedirectResponse = true
	_, err := Case(doSearch, limit, offset, query, orderField, orderBy)
	testParams.httpRedirectResponse = false

	expectedErrorStr := "Get /page1.html: stopped after 10 redirects"
	if err != nil {
		if err.Error() != expectedErrorStr {
			t.Error("Expected", expectedErrorStr, "got error:", err)
		}
	} else {
		t.Error("Expected error:", expectedErrorStr, "got nothing")
	}
}

func TestInvalidJSON(t *testing.T) {
	limit := 10
	offset := 0
	query := "Boyd"
	orderField := "id"
	orderBy := 1

	testParams.invalidJSON = true
	_, err := Case(doSearch, limit, offset, query, orderField, orderBy)
	testParams.invalidJSON = false

	expectedErrorStr := "invalid character '{' after top-level value"
	if err != nil {
		if err.Error() != expectedErrorStr {
			t.Error("Expected", expectedErrorStr, "got error:", err)
		}
	} else {
		t.Error("Expected error:", expectedErrorStr, "got nothing")
	}
}

func TestInvalidHost(t *testing.T) {
	limit := 10
	offset := 0
	query := "aaaaa"
	orderField := "id"
	orderBy := 1

	_, err := doSearch("~qwerty", limit, offset, query, orderField, orderBy)

	expectedErrorStr := "Get ~qwerty?limit=11&offset=0&order_by=1&order_field=id&query=aaaaa: unsupported protocol scheme \"\""
	if err != nil {
		if err.Error() != expectedErrorStr {
			t.Error("Expected", expectedErrorStr, "got error:", err)
		}
	} else {
		t.Error("Expected error:", expectedErrorStr, "got nothing")
	}
}
