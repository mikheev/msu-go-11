package main

//import "fmt"
import "bufio"
import "os"
import "sort"
import "strings"
import "strconv"
import "time"

//import "encoding/json"
import "gopkg.in/telegram-bot-api.v4"

//import "io/ioutil"
import "log"
import "net/http"

///////////////////////////////////////////////////////////////////
/*----------------- Глобальные параметры ----------------------- */
///////////////////////////////////////////////////////////////////
var gameOver bool

var roomList map[string]roomType
var players map[string]*playerBEType
var commandList map[string]commandType

var userNotActiveTimeout time.Duration // Секунд
var adminName string

var bot *tgbotapi.BotAPI

// При старте приложения, оно скажет телеграму ходить с обновлениями по этому URL
const WebhookURL = "https://mikheev-msu-go.herokuapp.com/bot"

///////////////////////////////////////////////////////////////////
/*----------------------- Игроки ------------------------------- */
///////////////////////////////////////////////////////////////////
//--------------------- Player backend ----------------------//
type playerBEType struct {
	name         string
	id           int64
	itemList     map[string]itemType
	wearableList map[string]wearableType
	currentRoom  string
	destroyTimer *time.Timer
	newRoom      bool
	alive        bool
}

func addPlayer(newPlayerName string, id int64) {
	var newPlayerBE playerBEType
	newPlayerBE.name = newPlayerName
	newPlayerBE.id = id

	newPlayerBE.itemList = make(map[string]itemType)
	newPlayerBE.wearableList = make(map[string]wearableType)

	newPlayerBE.currentRoom = "кухня"
	newPlayerBE.newRoom = true // Ещё не осмотрелись
	roomList["кухня"].playersInside[newPlayerName] = true
	newPlayerBE.destroyTimer = time.NewTimer(userNotActiveTimeout)

	players[newPlayerName] = &newPlayerBE
	go players[newPlayerName].deletePlayerIfNotActive()

	players[newPlayerName].sendMessage("Ты в игре!\nВводи команды.\n")
}

func (player playerBEType) deletePlayerIfNotActive() {
	// Здесь зависнем до тех пор, пока таймер не досчитает до нуля.
	println("Таймер запушен")
	<-player.destroyTimer.C
    player.sendMessage("Ты ничего не делал в течение "+strconv.Itoa(int(userNotActiveTimeout)/int(time.Minute))+" минут, поэтому ты выходишь из игры.")

	for _, item := range player.itemList {
		(*item.initialPlace).items[item.name] = item
	}

	for _, wearable := range player.wearableList {
		(*wearable.initialPlace).wearables[wearable.name] = wearable
	}

	delete(players, player.name)
}

func (player playerBEType) sendMessage(messageString string) {
	message := tgbotapi.NewMessage(player.id, messageString)
	bot.Send(message)
}

///////////////////////////////////////////////////////////////////
/*-----------------  Инициализация игры   ---------------------- */
///////////////////////////////////////////////////////////////////
func initGame() {
	initCommandList()
	initRoomList()
	players = make(map[string]*playerBEType)

	//userNotActiveTimeout = 60 * 15 * time.Second // Секунд
    userNotActiveTimeout = 60 * 1 * time.Second // Секунд
	gameOver = false

	adminName = "Nikita"

	//println("Привет!")
	//println("Игра началась. Введи команду")
}

func resetGame() {
	for command := range commandList {
		delete(commandList, command)
	}
	for room := range roomList {
		delete(roomList, room)
	}

	for playerName := range players {
		players[playerName].sendMessage("Игра перезагружается.\nТы будешь перемещён на кухню.")
		delete(players, playerName)
	}

	initCommandList()
	initRoomList()
}

///////////////////////////////////////////////////////////////////
/*--------------------- Команды -------------------------------- */
///////////////////////////////////////////////////////////////////
type commandType struct {
	// Имя, по которому будут вызывть команду
	name string
	// Доступна ли команда в данный момент игроку
	active bool
	// Обработчик команды
	// 0-й аргумент - имя команды, дальше список аргументов произвольной длины
	handler func(args []string, player *playerBEType) string
}

func getUserCommand(playerName string) {
	reader := bufio.NewReader(os.Stdin)

	rawReceivedCommand, _ := reader.ReadString('\n')
	rawReceivedCommand = rawReceivedCommand[:len(rawReceivedCommand)-2] // Избавляемся от "\r\n"

	_, ok := players[playerName]
	if !ok {
		// Такого игрока в игре нет.
		// Нужно создаваться
		addPlayer(playerName, 42)
	} else {
		// Сбрасываем таймер самоуничтожения игрока
		players[playerName].destroyTimer.Reset(userNotActiveTimeout)
	}

	response := handleCommand(rawReceivedCommand, players[playerName])
	println(response)
}

func handleCommand(rawReceivedCommand string, player *playerBEType) string {
	receivedCommand := strings.Split(rawReceivedCommand, " ")

	command, ok := commandList[receivedCommand[0]] // 0-й аргумент - имя команды
	if !ok {
		response := "неизвестная команда"
		return response
	}

	if !command.active {
		response := "команда недоступна"
		return response
	}

	return command.handler(receivedCommand, player)
}

func commandsAvailable(playerName string) []string {
	var commands []string
	player, ok := players[playerName]
	if !ok {
		return commands
	}
	commands = append(commands, "осмотреться")

	for gate := range roomList[player.currentRoom].gates {
		commands = append(commands, "идти "+gate)
	}

	// Если мы ещё не осмотрелись, предметов пока не видно
	if !player.newRoom {
		for _, place := range roomList[player.currentRoom].places {
			for _, item := range place.items {
				commands = append(commands, "взять "+item.name)
			}
		}

		for _, place := range roomList[player.currentRoom].places {
			for _, wearable := range place.wearables {
				commands = append(commands, "одеть "+wearable.name)
			}
		}

		for _, item := range player.itemList {
			for _, place := range roomList[player.currentRoom].places {
				for _, item2 := range place.items {
					commands = append(commands, "применить "+item.name+" "+item2.name)
				}
			}
		}
	}
	if player.name == adminName {
		commands = append(commands, "сбросить_игру")
	}

	return commands
}

//============================ Инициализация команд =======================//
func initCommandList() {
	commandList = make(map[string]commandType)
	var newCommand commandType
	//--------------------------- осмотреться ---------------------------------//
	newCommand.name = "осмотреться"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		cntr := 0
		if len(args) != 1 {
			response = response + "неверное число аргументов"
			return response
		}

		// Сообщение комнаты
		if len(roomList[player.currentRoom].lookAroundMessage) > 0 {
			response = response + roomList[player.currentRoom].getLookAroundMessage(roomList[player.currentRoom], player)
			cntr++
		}

		// Доступные предметы
		response = response + roomList[player.currentRoom].itemsAvailable(roomList[player.currentRoom])
		if len(response) > 0 {
			if response[len(response)-1] != ' ' {
				response = response + " "
			}
		}
		response = response + roomList[player.currentRoom].gatesAvailable(roomList[player.currentRoom])

		// Другие игроки
		if len(roomList[player.currentRoom].playersInside) > 1 {
			response = response + ". Кроме вас тут ещё "
			cnt := 1
			for otherPlayer := range roomList[player.currentRoom].playersInside {
				if otherPlayer != player.name {
					response = response + otherPlayer
					cnt++
					if cnt < len(roomList[player.currentRoom].playersInside) {
						response = response + ", "
					}
				}
			}
		}

		player.newRoom = false

		return response
	}
	commandList[newCommand.name] = newCommand

	//------------------------------ идти ------------------------------------//
	newCommand.name = "идти"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		if len(args) != 2 {
			response = response + "неверное число аргументов"
			return response
		}

		// Такого пути нет
		gate, ok := roomList[player.currentRoom].gates[args[1]]
		if !ok {
			response = response + "нет пути в " + args[1]
			return response
		}

		// Путь есть, но закрыт
		if !gate.isOpen {
			response = response + gate.closedMessage
			return response
		}

		// Переходим
		delete(roomList[player.currentRoom].playersInside, player.name)
		player.currentRoom = gate.destination
		roomList[gate.destination].playersInside[player.name] = true

		response = response + roomList[player.currentRoom].comeInMessage
		if len(response) > 0 {
			response = response + " "
		}
		response = response + roomList[player.currentRoom].gatesAvailable(roomList[player.currentRoom])

		player.newRoom = true

		return response
	}
	commandList[newCommand.name] = newCommand

	//------------------------------ одеть ------------------------------------//
	newCommand.name = "одеть"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		if len(args) != 2 {
			response = response + "неверное число аргументов"
			return response
		}

		// Ищем вещь во всех местах в комнате
		for _, place := range roomList[player.currentRoom].places {
			wearable, ok := place.wearables[args[1]]
			if ok {
				player.wearableList[args[1]] = wearable
				delete(place.wearables, args[1])
				response = response + "вы одели: " + args[1]
				return response
			}
		}

		// Возможно, игрок перепутал носимую вещь с неносимой
		for _, place := range roomList[player.currentRoom].places {
			_, ok := place.items[args[1]]
			if ok {
				response = response + "нельзя одеть "
				response = response + args[1]
				response = response + ", попробуй взять"
				return response
			}
		}

		// Ничего не нашли
		response = response + "в " + roomList[player.currentRoom].name + " нет " + args[1]
		return response
	}
	commandList[newCommand.name] = newCommand

	//------------------------------ взять ------------------------------------//
	newCommand.name = "взять"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		if len(args) != 2 {
			response = response + "неверное число аргументов"
			return response
		}

		_, haveBag := player.wearableList["рюкзак"]
		if !haveBag {
			response = response + "некуда класть"
			return response
		}

		// Ищем вещь во всех местах в комнате
		for _, place := range roomList[player.currentRoom].places {
			item, ok := place.items[args[1]]
			if ok {
				if !item.canTake {
					response = response + "нельзя взять " + args[1]
					return response
				}

				player.itemList[args[1]] = item
				delete(place.items, args[1])
				response = response + "предмет добавлен в инвентарь: " + args[1]
				return response
			}
		}

		// Возможно, игрок перепутал носимую вещь с неносимой
		for _, place := range roomList[player.currentRoom].places {
			_, ok := place.wearables[args[1]]
			if ok {
				response = response + "нельзя взять "
				response = response + args[1]
				response = response + ", попробуй одеть"
				return response
			}
		}

		// Ничего не нашли
		response = response + "нет такого"
		return response
	}
	commandList[newCommand.name] = newCommand

	//------------------------------ применить ------------------------------------//
	newCommand.name = "применить"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		if len(args) != 3 {
			response = response + "неверное число аргументов"
			return response
		}

		// Ищем первый предмет в инвентаре
		item1, ok := player.itemList[args[1]]
		if !ok {
			response = response + "нет предмета в инвентаре - " + args[1]
			return response
		}

		// Ищем второй предмет в комнате
		var ok2 bool
		for _, place := range roomList[player.currentRoom].places {
			_, ok2 = place.items[args[2]]
			if ok2 {
				break
			}
		}

		if !ok2 {
			response = response + "не к чему применить"
			return response
		}

		handler, ok3 := item1.applications[args[2]]
		if !ok3 {
			response = response + "Нельзя применить " + args[1] + " к " + args[2]
			return response
		}

		response = response + handler(0)
		return response

	}
	commandList[newCommand.name] = newCommand

	//------------------------------ сказать ------------------------------------//
	newCommand.name = "сказать"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""

		message := player.name + " говорит: " + strings.Join(args[1:], " ")

		for pName := range roomList[player.currentRoom].playersInside {
			players[pName].sendMessage(message)
		}

		return response
	}
	commandList[newCommand.name] = newCommand

	//----------------------- сказать_игроку ------------------------------------//
	newCommand.name = "сказать_игроку"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		message := ""
		if len(args) < 2 {
			response = response + "неверное число аргументов"
		} else if len(args) == 2 {
			message = message + player.name + " выразительно молчит, смотря на вас"
		} else {
			message = message + player.name + " говорит вам: " + strings.Join(args[2:], " ")
		}

		_, ok := roomList[player.currentRoom].playersInside[args[1]]
		if ok {
			players[args[1]].sendMessage(message)
		} else {
			response = response + "тут нет такого игрока"
		}

		return response
	}
	commandList[newCommand.name] = newCommand

	//------------------------------ сбросить_игру ------------------------------------//
	newCommand.name = "сбросить_игру"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		if player.name != adminName {
			// Команда доступна только администратору
			response = response + "неизвестная команда"
			return response
		}

		resetGame()
		response = response + "игра сброшена"
		//gameOver = true
		return response
	}
	commandList[newCommand.name] = newCommand

	//------------------------------ завершить_игру ------------------------------------//
	newCommand.name = "завершить_игру"
	newCommand.active = true
	newCommand.handler = func(args []string, player *playerBEType) string {
		response := ""
		player.destroyTimer.Reset(time.Millisecond)
		response = response + "игра завершена"
		//gameOver = true
		return response
	}
	commandList[newCommand.name] = newCommand
}

///////////////////////////////////////////////////////////////////
/*-------------------- Комнаты ----------------------------------*/
///////////////////////////////////////////////////////////////////
type roomType struct {
	name                 string
	comeInMessage        string
	lookAroundMessage    string
	getLookAroundMessage func(r roomType, player *playerBEType) string
	active               bool // Доступна ли комната в данный момент игроку
	places               map[string]*placeType
	itemsAvailable       func(r roomType) string
	gates                map[string]gateType // Проходы в комнаты, в которые можно перейти из данной
	gatesAvailable       func(r roomType) string
	playersInside        map[string]bool
}

// ============= Стандартные методы комнат ======================//
// -- Получить информацию о комнате для команды "осмотреться" -- //
func defaultGetLookAroundMessage(r roomType, _ *playerBEType) string {
	return r.lookAroundMessage
}

// --------- Получить список возможных переходов --------------- //
func defaultGatesAvailable(r roomType) string {
	response := ""
	if len(r.gates) > 0 {
		response = response + "можно пройти - "

		var gateDests []string
		for _, gate := range r.gates {
			gateDests = append(gateDests, gate.destination)
		}
		sort.Strings(gateDests)
		for idx, gateDest := range gateDests {
			response = response + gateDest
			if idx < (len(gateDests) - 1) {
				response = response + ", "
			}
		}
	}
	return response
}

// --------- Получить список доступных предметов --------------- //
func defaultItemsAvailable(r roomType) string {
	response := ""
	cntp := 0
	tempResponse := ""
	for _, place := range r.places {
		if len(place.items)+len(place.wearables) > 0 {
			if (cntp < len(r.places)) && (cntp > 0) {
				tempResponse = tempResponse + ", "
			}
			tempResponse = tempResponse + place.name
			if len(place.items)+len(place.wearables) > 1 {
				tempResponse = tempResponse + ": "
			} else {
				tempResponse = tempResponse + " - "
			}
			cnt := 0
			for _, item := range place.items {
				tempResponse = tempResponse + item.name
				cnt++
				if cnt < len(place.items)+len(place.wearables) {
					tempResponse = tempResponse + ", "
				}
			}
			for _, wearable := range place.wearables {
				tempResponse = tempResponse + wearable.name
				cnt++
			}
			if cnt < len(place.items)+len(place.wearables) {
				tempResponse = tempResponse + ", "
			}

		}
		cntp++
	}
	if cntp > 0 {
		if tempResponse != "" {
			response = response + tempResponse + "."
		} else {
			response = response + "пустая комната."
		}
	}
	return response
}

func initRoom(newRoom roomType) {
	newRoom.getLookAroundMessage = defaultGetLookAroundMessage
	newRoom.gates = make(map[string]gateType)
	newRoom.places = make(map[string]*placeType)
	newRoom.itemsAvailable = defaultItemsAvailable
	newRoom.active = true
	newRoom.gatesAvailable = defaultGatesAvailable
	newRoom.playersInside = make(map[string]bool)

	roomList[newRoom.name] = newRoom
}

// --------------- Места и предметы в комнатах --------------------//
type placeType struct {
	name      string
	items     map[string]itemType
	wearables map[string]wearableType
}

func initPlace(newPlace placeType, room roomType) {
	newPlace.items = make(map[string]itemType)
	newPlace.wearables = make(map[string]wearableType)
	room.places[newPlace.name] = &newPlace
}

type gateType struct {
	destination   string
	isOpen        bool
	closedMessage string
}

// --------------- Инициализация комнат --------------------//
func initRoomList() {
	roomList = make(map[string]roomType)
	var newRoom roomType
	var newPlace placeType
	var newItem itemType
	var newWearable wearableType

	//----------------------- Описание комнат (без проходов) ------------------
	newRoom.name = "кухня"
	newRoom.comeInMessage = "кухня, ничего интересного."
	newRoom.lookAroundMessage = "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ."
	initRoom(newRoom)
	// Перегружаем defaultGetLookAroundMessage
	newRoom = roomList["кухня"]
	newRoom.getLookAroundMessage = func(r roomType, player *playerBEType) string {
		response := ""
		_, ok := player.wearableList["рюкзак"]
		if !ok {
			response = response + "ты находишься на кухне, на столе чай, надо собрать рюкзак и идти в универ."
		} else {
			response = response + "ты находишься на кухне, на столе чай, надо идти в универ."
		}
		return response
	}
	roomList["кухня"] = newRoom

	newRoom.name = "коридор"
	newRoom.comeInMessage = "ничего интересного."
	newRoom.lookAroundMessage = "ничего интересного."
	initRoom(newRoom)
	// Перешрузка gatesAvailable, чтобы обеспечить правильный порядок
	newRoom = roomList["коридор"]
	newRoom.gatesAvailable = func(r roomType) string {
		return "можно пройти - кухня, комната, улица"
	}
	roomList["коридор"] = newRoom

	// Инициализация мест
	newPlace.name = "в конце коридорa"
	initPlace(newPlace, roomList["коридор"])
	// Инициализация вещей
	newItem.name = "дверь"
	newItem.canTake = false
	newItem.applications = make(map[string]func(interface{}) string)
	newItem.initialPlace = roomList["коридор"].places["в конце коридорa"]
	roomList["коридор"].places["в конце коридорa"].items[newItem.name] = newItem

	newRoom.name = "комната"
	newRoom.comeInMessage = "ты в своей комнате."
	newRoom.lookAroundMessage = ""
	initRoom(newRoom)
	// Инициализация мест
	newPlace.name = "на столе"
	initPlace(newPlace, roomList["комната"])
	newPlace.name = "на стуле"
	initPlace(newPlace, roomList["комната"])
	// Инициализация вещей
	// Ключи
	newItem.name = "ключи"
	newItem.canTake = true
	newItem.applications = make(map[string]func(interface{}) string)
	newItem.initialPlace = roomList["комната"].places["на столе"]
	roomList["комната"].places["на столе"].items[newItem.name] = newItem
	// Конспекты
	newItem.name = "конспекты"
	newItem.canTake = true
	newItem.applications = make(map[string]func(interface{}) string)
	newItem.initialPlace = roomList["комната"].places["на столе"]
	roomList["комната"].places["на столе"].items[newItem.name] = newItem
	// Рюкзак
	newWearable.name = "рюкзак"
	newWearable.initialPlace = roomList["комната"].places["на стуле"]
	roomList["комната"].places["на стуле"].wearables[newWearable.name] = newWearable
	// Перегрузка itemsAvailable, чтобы обеспечить ":"и "-" в правильных местах
	newRoom = roomList["комната"]
	newRoom.itemsAvailable = func(r roomType) string {
		response := ""
		if len(r.places["на столе"].items) == 2 {
			response = response + "на столе: ключи, конспекты"
		} else if len(r.places["на столе"].items) == 1 {
			response = response + "на столе: "
			for _, item := range r.places["на столе"].items {
				response = response + item.name
			}
		}

		if len(r.places["на стуле"].wearables) > 0 {
			if len(response) > 0 {
				response = response + ", "
			}
			response = response + "на стуле - рюкзак"
		}

		if len(response) > 0 {
			response = response + "."
		} else {
			response = "пустая комната."
		}

		return response
	}
	roomList["комната"] = newRoom

	newRoom.name = "улица"
	newRoom.comeInMessage = "на улице весна."
	newRoom.lookAroundMessage = "ничего интересного."
	initRoom(newRoom)

	newRoom.name = "домой"
	newRoom.comeInMessage = "ты дома."
	newRoom.lookAroundMessage = "ничего интересного."
	initRoom(newRoom)

	//---------------- Описание проходов между комнатами ------------------------
	roomList["кухня"].gates["коридор"] = gateType{destination: "коридор", isOpen: true, closedMessage: ""}
	roomList["коридор"].gates["кухня"] = gateType{destination: "кухня", isOpen: true, closedMessage: ""}

	roomList["комната"].gates["коридор"] = gateType{destination: "коридор", isOpen: true, closedMessage: ""}
	roomList["коридор"].gates["комната"] = gateType{destination: "комната", isOpen: true, closedMessage: ""}

	roomList["коридор"].gates["улица"] = gateType{destination: "улица", isOpen: false, closedMessage: "дверь закрыта"}
	roomList["улица"].gates["домой"] = gateType{destination: "домой", isOpen: true, closedMessage: ""}

	//----------------- Дополнительные сценарии -------------//
	roomList["комната"].places["на столе"].items["ключи"].applications["дверь"] = func(_ interface{}) string {
		gate := roomList["коридор"].gates["улица"]
		gate.isOpen = true
		roomList["коридор"].gates["улица"] = gate
		return "дверь открыта"

	}
}

///////////////////////////////////////////////////////////////////
/*-------------------- Инвентарь --------------------------------*/
///////////////////////////////////////////////////////////////////
type itemType struct {
	name         string
	canTake      bool
	applications map[string]func(interface{}) string
	initialPlace *placeType
}

type wearableType struct {
	name         string
	initialPlace *placeType
}

///////////////////////////////////////////////////////////////////
/*-------------------- main -------------------------------------*/
///////////////////////////////////////////////////////////////////

func main() {
	initGame()

	// Heroku прокидывает порт для приложения в переменную окружения PORT
	var err error
	port := os.Getenv("PORT")
	bot, err = tgbotapi.NewBotAPI("364918543:AAHGTAHlvTA0PogUjzkamJvmnh4Qd2xeq-E")
	if err != nil {
		log.Fatal(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	// Устанавливаем вебхук
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(WebhookURL))
	if err != nil {
		log.Fatal(err)
	}

	updates := bot.ListenForWebhook("/bot")
	go http.ListenAndServe(":"+port, nil)

	// получаем все обновления из канала updates
	for update := range updates {
		var message tgbotapi.MessageConfig
		log.Println("received text: ", update.Message.Text)

		rawReceivedCommand := update.Message.Text
		playerName := update.Message.From.FirstName

		_, ok := players[playerName]
		if !ok {
			// Такого игрока в игре нет.
			// Нужно создаваться
			addPlayer(playerName, update.Message.Chat.ID)
		} else {
			// Сбрасываем таймер самоуничтожения игрока
			players[playerName].destroyTimer.Reset(userNotActiveTimeout)
		}

		response := handleCommand(rawReceivedCommand, players[playerName])
		message = tgbotapi.NewMessage(update.Message.Chat.ID, response)

		// В ответном сообщении просим показать клавиатуру
		newButtonRow := make([]tgbotapi.KeyboardButton, 1)
		var newButton tgbotapi.KeyboardButton
		commands := commandsAvailable(playerName)
		buttons := make([][]tgbotapi.KeyboardButton, len(commands))
		for idx, command := range commands {
			newButton.Text = command
			newButtonRow[0] = newButton
			buttons[idx] = append(buttons[idx], newButton)
		}
		message.ReplyMarkup = tgbotapi.NewReplyKeyboard(buttons...)
		bot.Send(message)
	}

	/*
		playerName := "Nikita"

		for !gameOver {
			getUserCommand(playerName)
		}*/
}
