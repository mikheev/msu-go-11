package main

import "net/http"
import "io/ioutil"
import "strings"

// Crawl проходит по всем ссылкам переданного хоста
// и возвращает список страниц, доступных по этим ссылкам
// в пределах данного хоста.
func Crawl(host string) []string {
	var sitePages []string
	println("Host:", host)
	page := "/"

	analizePage(host, page, &sitePages, 0)

	return sitePages
}

func getPage(url *string) (string, int) {
	resp, err := http.Get(*url)
	if err != nil {
		println("Http.get(", url, ") error: ", err.Error())
		return "", -1
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	// При редиректе урл мог измениться
	*url = resp.Request.URL.String()

	return string(body), resp.StatusCode
}

func getFirstLink(body string) (string, string) {

	idx := strings.Index(body, "<a href=\"")
	if idx == -1 {
		return "", ""
	}

	body = body[(idx + len("<a href=\"")):]

	idx = strings.Index(body, "\"")
	url := body[:idx]
	body = body[idx+2:]

	return url, body
}

func analizePage(host string, page string, sitePages *[]string, level int) {
	var pageNext string
	var url string

	url = host + page
	body, code := getPage(&url)
	//sprintln(level, " ", code, ":", url)

	if code != 200 {
		return
	}

	if url != host+page {
		// Редирект детектед
		if strings.Contains(url, host) {
			// Редирект  на тот же хост
			page = url[len(host):]
			if contains(sitePages, page) {
				// Мы здесь уже были
				return
			}
		} else {
			// Попали на другой хост
			return
		}
	}

	*sitePages = append(*sitePages, page)

	pageNext, body = getFirstLink(body)
	for body != "" {
		// Проверяем, что мы ещё не были на этой странице
		if !contains(sitePages, pageNext) {
			// Проверяем, что нам не подсунули новый хост
			if !strings.Contains(pageNext, "http") {
				if (len(page) > 0) && (pageNext[0] != '/') {
					pageNext = "/" + pageNext
				}
				analizePage(host, pageNext, sitePages, level+1)
			}
		}
		pageNext, body = getFirstLink(body)
	}
	return
}

func contains(s *[]string, sample string) bool {
	for _, element := range *s {
		if element == sample {
			return true
		}
	}
	return false
}

func main() {
	var sitePages []string

	sitePages = Crawl("http://gotest")

	println("-----------")
	for _, url := range sitePages {
		println(url)
	}

}
