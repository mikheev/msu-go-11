package main

import (
	"fmt"
)

// TODO: Реализовать вычисление Квадратного корня
func Sqrt(A float64) float64 {
	// Начальное приближение
	var x = A         // Type same as A - float64
	var xPrev float64 // Default value is 0.
	eps := 1.E-7
	for (x-xPrev)*(x-xPrev) > eps {
		xPrev = x
		x = (xPrev + A/xPrev) / 2.
	}

	return x
}

func main() {
	fmt.Println(Sqrt(2))
}
