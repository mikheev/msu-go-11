package main

func showMeTheType(i interface{}) string {

	var ok bool
	_, ok = i.(int)
	if ok {
		return "int"
	}

	_, ok = i.(int32)
	if ok {
		return "int32"
	}

	_, ok = i.(uint)
	if ok {
		return "uint"
	}

	_, ok = i.(int8)
	if ok {
		return "int8"
	}

	_, ok = i.(float64)
	if ok {
		return "float64"
	}

	_, ok = i.(string)
	if ok {
		return "string"
	}

	_, ok = i.([]int)
	if ok {
		return "[]int"
	}

	_, ok = i.(map[string]bool)
	if ok {
		return "map[string]bool"
	}

	return "Unknown type"
}

func main() {

}
