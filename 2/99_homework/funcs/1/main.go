package main

import "fmt"
import "strconv"
import "strings"

type memoizeFunction func(int, ...int) interface{}

// TODO реализовать
var fibonacci memoizeFunction
var romanForDecimal memoizeFunction

//TODO Write memoization function

func memoize(function memoizeFunction) memoizeFunction {
	var Storage map[string]interface{}
	Storage = make(map[string]interface{})
	return func(n int, sn ...int) interface{} {

		// Calculate hash from set of arguments
		var slTemp []string
		slTemp = append(slTemp, strconv.Itoa(n))
		for _, num := range sn {
			slTemp = append(slTemp, strconv.Itoa(num))
		}

		key := strings.Join(slTemp, "+")
		res, ok := Storage[key]
		if ok {
			return res
		}
		// Element not found
		res = function(n, sn...)
		Storage[key] = res
		return res
	}
}

// TODO обернуть функции fibonacci и roman в memoize
func init() {

	fibonacci = func(n int, _ ...int) interface{} {
		if n == 0 {
			return 0
		}
		if n == 1 {
			return 1
		}

		return fibonacci(n-1).(int) + fibonacci(n-2).(int)
	}

	romanForDecimal = func(n int, _ ...int) interface{} {
		s := ""

		if n < 10 {
			switch n {
			case 1:
				s += "I"
			case 2:
				s += "II"
			case 3:
				s += "III"
			case 4:
				s += "IV"
			case 5:
				s += "V"
			case 6:
				s += "Vi"
			case 7:
				s += "VII"
			case 8:
				s += "VIII"
			case 9:
				s += "IX"
			}
			return s
		}

		if n < 100 {
			switch (n - n%10) / 10 {
			case 1:
				s += "X"
			case 2:
				s += "XX"
			case 3:
				s += "XXX"
			case 4:
				s += "XL"
			case 5:
				s += "L"
			case 6:
				s += "LX"
			case 7:
				s += "LXX"
			case 8:
				s += "LXXX"
			case 9:
				s += "XC"
			}
			s = s + romanForDecimal(n%10).(string)
			return s
		}

		if n < 1000 {
			switch (n - n%100) / 100 {
			case 1:
				s += "C"
			case 2:
				s += "CC"
			case 3:
				s += "CCC"
			case 4:
				s += "CD"
			case 5:
				s += "D"
			case 6:
				s += "DC"
			case 7:
				s += "DCC"
			case 8:
				s += "DCCC"
			case 9:
				s += "CM"
			}
			s = s + romanForDecimal(n%100).(string)
			return s
		}

		if n < 4000 {
			switch (n - n%1000) / 1000 {
			case 1:
				s += "M"
			case 2:
				s += "MM"
			case 3:
				s += "MMM"
			}
			s = s + romanForDecimal(n%1000).(string)
			return s
		}

		return "Error"
	}

	fibonacci = memoize(fibonacci)
	romanForDecimal = memoize(romanForDecimal)
}

func main() {
	fmt.Println("Fibonacci(45) =", fibonacci(45).(int))
	for _, x := range []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
		14, 15, 16, 17, 18, 19, 20, 25, 30, 40, 50, 60, 69, 70, 80,
		90, 99, 100, 200, 300, 400, 500, 600, 666, 700, 800, 900,
		1000, 1009, 1444, 1666, 1945, 1997, 1999, 2000, 2008, 2010,
		2012, 2500, 3000, 3999} {
		fmt.Printf("%4d = %s\n", x, romanForDecimal(x).(string))
	}
}
