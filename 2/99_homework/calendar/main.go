package main

import (
	"fmt"
	"time"
)

type calendarClass struct {
	Year  int
	Month int
	Day   int
}

func (c *calendarClass) CurrentQuarter() int {
	if c.Month < 4 {
		return 1
	}

	if c.Month < 7 {
		return 2
	}

	if c.Month < 10 {
		return 3
	}

	return 4

}

func NewCalendar(t time.Time) calendarClass {
	var c calendarClass
	c.Year = t.Year()
	c.Month = int(t.Month())
	c.Day = t.Day()

	return c
}

func main() {
	parsed, _ := time.Parse("2006-01-02", fmt.Sprintf("2015-%s-15", "03"))
	fmt.Println(parsed)
}
